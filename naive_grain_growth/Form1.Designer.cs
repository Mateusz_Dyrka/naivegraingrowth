﻿namespace naive_grain_growth
{
    partial class canvas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cleanButton = new System.Windows.Forms.Button();
            this.startButton = new System.Windows.Forms.Button();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.checkedListBox2 = new System.Windows.Forms.CheckedListBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.setButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.stillStartButton = new System.Windows.Forms.Button();
            this.stillStopButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.grid_checkBox2 = new System.Windows.Forms.CheckBox();
            this.recrystallizationButton = new System.Windows.Forms.Button();
            this.monteCarloButton = new System.Windows.Forms.Button();
            this.oneMCstepButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cleanButton
            // 
            this.cleanButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.cleanButton.Location = new System.Drawing.Point(521, 418);
            this.cleanButton.Name = "cleanButton";
            this.cleanButton.Size = new System.Drawing.Size(75, 23);
            this.cleanButton.TabIndex = 0;
            this.cleanButton.Text = "Clean";
            this.cleanButton.UseVisualStyleBackColor = true;
            this.cleanButton.Click += new System.EventHandler(this.cleanButton_Click);
            // 
            // startButton
            // 
            this.startButton.ForeColor = System.Drawing.Color.Red;
            this.startButton.Location = new System.Drawing.Point(482, 389);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(75, 23);
            this.startButton.TabIndex = 1;
            this.startButton.Text = "Grow";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Items.AddRange(new object[] {
            "Moore",
            "von Neumann",
            "Heksagonal Left",
            "Heksagonal Right",
            "Heksagonal Rand",
            "Pentagonal Rand"});
            this.checkedListBox1.Location = new System.Drawing.Point(482, 36);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(161, 94);
            this.checkedListBox1.TabIndex = 2;
            this.checkedListBox1.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox1_ItemCheck);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(479, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Select the type of neighborhood";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(479, 245);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Set amount";
            // 
            // checkedListBox2
            // 
            this.checkedListBox2.FormattingEnabled = true;
            this.checkedListBox2.Items.AddRange(new object[] {
            "Losowe",
            "Równomierne",
            "Losowe z promieniem R"});
            this.checkedListBox2.Location = new System.Drawing.Point(481, 172);
            this.checkedListBox2.Name = "checkedListBox2";
            this.checkedListBox2.Size = new System.Drawing.Size(161, 64);
            this.checkedListBox2.TabIndex = 7;
            this.checkedListBox2.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox2_ItemCheck);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(545, 242);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(97, 20);
            this.textBox1.TabIndex = 8;
            // 
            // setButton
            // 
            this.setButton.ForeColor = System.Drawing.Color.Red;
            this.setButton.Location = new System.Drawing.Point(567, 389);
            this.setButton.Name = "setButton";
            this.setButton.Size = new System.Drawing.Size(75, 23);
            this.setButton.TabIndex = 9;
            this.setButton.Text = "Set location";
            this.setButton.UseVisualStyleBackColor = true;
            this.setButton.Click += new System.EventHandler(this.setButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(479, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Select the type of location";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(481, 136);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(81, 17);
            this.checkBox1.TabIndex = 11;
            this.checkBox1.Text = "Periodic BC";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(545, 265);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(97, 20);
            this.textBox2.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(479, 268);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Set radius";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(505, 298);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(132, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Ciągłe losowanie (0.5 sec)";
            // 
            // stillStartButton
            // 
            this.stillStartButton.Location = new System.Drawing.Point(481, 314);
            this.stillStartButton.Name = "stillStartButton";
            this.stillStartButton.Size = new System.Drawing.Size(76, 23);
            this.stillStartButton.TabIndex = 16;
            this.stillStartButton.Text = "Start";
            this.stillStartButton.UseVisualStyleBackColor = true;
            this.stillStartButton.Click += new System.EventHandler(this.stillStartButton_Click);
            // 
            // stillStopButton
            // 
            this.stillStopButton.Location = new System.Drawing.Point(565, 315);
            this.stillStopButton.Name = "stillStopButton";
            this.stillStopButton.Size = new System.Drawing.Size(78, 22);
            this.stillStopButton.TabIndex = 17;
            this.stillStopButton.Text = "Stop";
            this.stillStopButton.UseVisualStyleBackColor = true;
            this.stillStopButton.Click += new System.EventHandler(this.stillStopButton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(542, 350);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "MENU";
            // 
            // grid_checkBox2
            // 
            this.grid_checkBox2.AutoSize = true;
            this.grid_checkBox2.Location = new System.Drawing.Point(511, 366);
            this.grid_checkBox2.Name = "grid_checkBox2";
            this.grid_checkBox2.Size = new System.Drawing.Size(98, 17);
            this.grid_checkBox2.TabIndex = 19;
            this.grid_checkBox2.Text = "Show/hide grid";
            this.grid_checkBox2.UseVisualStyleBackColor = true;
            this.grid_checkBox2.CheckedChanged += new System.EventHandler(this.grid_checkBox2_CheckedChanged);
            // 
            // recrystallizationButton
            // 
            this.recrystallizationButton.BackColor = System.Drawing.SystemColors.Control;
            this.recrystallizationButton.ForeColor = System.Drawing.Color.Red;
            this.recrystallizationButton.Location = new System.Drawing.Point(483, 460);
            this.recrystallizationButton.Name = "recrystallizationButton";
            this.recrystallizationButton.Size = new System.Drawing.Size(161, 33);
            this.recrystallizationButton.TabIndex = 20;
            this.recrystallizationButton.Text = "Recrystallization";
            this.recrystallizationButton.UseVisualStyleBackColor = false;
            this.recrystallizationButton.Click += new System.EventHandler(this.recrystallizationButton_Click);
            // 
            // monteCarloButton
            // 
            this.monteCarloButton.ForeColor = System.Drawing.Color.Red;
            this.monteCarloButton.Location = new System.Drawing.Point(483, 513);
            this.monteCarloButton.Name = "monteCarloButton";
            this.monteCarloButton.Size = new System.Drawing.Size(160, 23);
            this.monteCarloButton.TabIndex = 21;
            this.monteCarloButton.Text = "Monte Carlo - set map";
            this.monteCarloButton.UseVisualStyleBackColor = true;
            this.monteCarloButton.Click += new System.EventHandler(this.monteCarloButton_Click);
            // 
            // oneMCstepButton
            // 
            this.oneMCstepButton.ForeColor = System.Drawing.Color.Red;
            this.oneMCstepButton.Location = new System.Drawing.Point(482, 543);
            this.oneMCstepButton.Name = "oneMCstepButton";
            this.oneMCstepButton.Size = new System.Drawing.Size(161, 23);
            this.oneMCstepButton.TabIndex = 22;
            this.oneMCstepButton.Text = "Let the magic happen!";
            this.oneMCstepButton.UseVisualStyleBackColor = true;
            this.oneMCstepButton.Click += new System.EventHandler(this.oneMCstepButton_Click);
            // 
            // canvas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 577);
            this.Controls.Add(this.oneMCstepButton);
            this.Controls.Add(this.monteCarloButton);
            this.Controls.Add(this.recrystallizationButton);
            this.Controls.Add(this.grid_checkBox2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.stillStopButton);
            this.Controls.Add(this.stillStartButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.setButton);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.checkedListBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkedListBox1);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.cleanButton);
            this.Name = "canvas";
            this.Text = "Form1";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.canvas_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cleanButton;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox checkedListBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button setButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button stillStartButton;
        private System.Windows.Forms.Button stillStopButton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox grid_checkBox2;
        private System.Windows.Forms.Button recrystallizationButton;
        private System.Windows.Forms.Button monteCarloButton;
        private System.Windows.Forms.Button oneMCstepButton;
    }
}

