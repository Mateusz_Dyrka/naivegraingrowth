﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using System.Threading;
using System.Security.Cryptography;

namespace naive_grain_growth
{
    public partial class canvas : Form
    {
        public static int n = 15;      //kolumny 32
        public static int m = 15;      //wiersze 32
        public Cell[,] tab;
        double mouse_X, mouse_Y;
        int index_X, index_Y;
        public int nbhdChoice = 0;
        public int locationChoice = 0;
        bool setBC = false;
        bool gridFlag = false;
        bool monteFlag = true;
        Random random = new Random();
        List<Grain> grainList = new List<Grain>();
        List<Grain> randomMCGrainList = new List<Grain>();
        Dictionary<Grain, Color> changeRecrystalizedGrainColor = new Dictionary<Grain, Color>();                     
        Color nbhdColor = Color.Empty;                                            //kolor przy zmianie kolorow dla rekrystalizacji
        System.Timers.Timer randTimer;
        System.Timers.Timer stillTimer;
        private int timeLeft = 10; //for timer
        bool timerFlag = true;
        bool stillFlag = true;
        private Graphics gObject;
        double A = 86710969050178.5;
        double B = 9.41268203527779;
        public double time = 0;
        public double step = 0.001;
        double ro = 0;
        double criticalRo = 4215840142323.42 / (n * m);

        public canvas()
        {
            gObject = this.CreateGraphics();
            Pen pen = new Pen(Color.Black, 1);
            tab = new Cell[n, m];
            InitializeComponent();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    if(!gridFlag)
                        gObject.DrawRectangle(pen, i * 8, j * 8, 8, 8);
                    tab[i, j] = new Cell();
                }
            }
            cleaner(n, m);
        }

        private void canvas_Paint(object sender, PaintEventArgs e)
        {
            Pen pen = new Pen(Color.Black, 1);
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    if (gridFlag)
                        gObject.DrawRectangle(pen, i * 8, j * 8, 8, 8);
                    if (!(tab[i, j].isEmpty()))
                    {
                        Color color = tab[i, j].getColor();
                        Brush brush = new SolidBrush(color);
                        gObject.FillRectangle(brush, i * 8, j * 8, 8, 8);
                    }
                }
            }
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            mouse_X = e.X;
            mouse_Y = e.Y;

            index_X = (int)mouse_X / 8;
            index_Y = (int)mouse_Y / 8;

            if (index_X < n && index_Y < m)
            {
                if (tab[index_X, index_Y].isEmpty())
                {
                    Color randomColor = Color.FromArgb(random.Next(255), random.Next(255),
                       random.Next(255));
                    Brush randBrush = new SolidBrush(randomColor);
                    tab[index_X, index_Y].setEmpty(false);
                    tab[index_X, index_Y].setColor(randomColor);
                    gObject.FillRectangle(randBrush, index_X * 8, index_Y * 8, 8, 8);
                }
            }
            this.Refresh();
        }

        private void doMagic()
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    if (!(tab[i, j].isEmpty()))
                    {
                        switch (nbhdChoice)
                        {
                            case 1:
                                MooreNbhd(i, j);
                                break;
                            case 2:
                                vonNeumannNbhd(i, j);
                                break;
                            case 3:
                                heksLeftNbhd(i, j);
                                break;
                            case 4:
                                heksRightNbhd(i, j);
                                break;
                            case 5:
                                heksRandNbhd(i, j);
                                break;
                            case 6:
                                pentRandNbhd(i, j);
                                break;
                        }
                    }
                }
            }

            if (checkedListBox1.SelectedIndex == -1)
                MessageBox.Show("Select the type of nbhd!");

            changeColours();
            this.Refresh();
        }

        private void changeColours()
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    if (tab[i, j].isAlmostDone())
                    {
                        tab[i, j].setEmpty(false);
                    }
                }
            }
        }
        public class Grain
        {
            public int i, j;
            public Grain(int i, int j)
            {
                this.i = i;
                this.j = j;
            }
        }

        private void MooreNbhd(int x, int y)
        {
            int[] xTab = { x - 1, x, x + 1 };       //tablica sasiadow
            int[] yTab = { y - 1, y, y + 1 };       //tablica sasiadow

            if (setBC)
            {
                if (xTab[0] == -1)
                    xTab[0] = n - 1;
                else if (xTab[2] == n)
                    xTab[2] = 0;

                if (yTab[0] == -1)
                    yTab[0] = m - 1;
                else if (yTab[2] == m)
                    yTab[2] = 0;
            }
            else
            {
                if (xTab[0] == -1)
                    xTab[0] = 0;
                else if (xTab[2] == n)
                    xTab[2] = n - 1;

                if (yTab[0] == -1)
                    yTab[0] = 0;
                else if (yTab[2] == m)
                    yTab[2] = m - 1;
            }
            Color ownerColor = tab[x, y].getColor();
            for (int i = 0; i < xTab.Length; i++)
            {
                for (int j = 0; j < yTab.Length; j++)
                {
                    if (!(xTab[i] == x && yTab[j] == y))
                    {
                        if (tab[xTab[i], yTab[j]].isEmpty())
                        {
                            tab[xTab[i], yTab[j]].setColor(ownerColor);
                            tab[xTab[i], yTab[j]].setAlmostDone(true);
                        }
                    }
                }
            }
        }
        private void vonNeumannNbhd(int x, int y)
        {
            int[] xTab = { x - 1, x, x + 1 };       //tablica sasiadow
            int[] yTab = { y - 1, y, y + 1 };       //tablica sasiadow

            if (setBC)
            {
                if (xTab[0] == -1)
                    xTab[0] = n - 1;
                else if (xTab[2] == n)
                    xTab[2] = 0;

                if (yTab[0] == -1)
                    yTab[0] = m - 1;
                else if (yTab[2] == m)
                    yTab[2] = 0;
            }

            else
            {
                if (xTab[0] == -1)
                    xTab[0] = 0;
                else if (xTab[2] == n)
                    xTab[2] = n - 1;

                if (yTab[0] == -1)
                    yTab[0] = 0;
                else if (yTab[2] == m)
                    yTab[2] = m - 1;
            }
            Color ownerColor = tab[x, y].getColor();
            for (int i = 0; i < xTab.Length; i++)
            {
                for (int j = 0; j < yTab.Length; j++)
                {
                    if (!(xTab[i] == x && yTab[j] == y) &&
                        !(xTab[i] == x - 1 && yTab[j] == y - 1) &&
                        !(xTab[i] == x + 1 && yTab[j] == y + 1) &&
                        !(xTab[i] == x - 1 && yTab[j] == y + 1) &&
                        !(xTab[i] == x + 1 && yTab[j] == y - 1))
                    {
                        if (tab[xTab[i], yTab[j]].isEmpty())
                        {
                            tab[xTab[i], yTab[j]].setColor(ownerColor);
                            tab[xTab[i], yTab[j]].setAlmostDone(true);
                        };
                    }
                }
            }
        }
        private void heksLeftNbhd(int x, int y)
        {
            int[] xTab = { x - 1, x, x + 1 };       //tablica sasiadow
            int[] yTab = { y - 1, y, y + 1 };       //tablica sasiadow

            if (setBC)
            {
                if (xTab[0] == -1)
                    xTab[0] = n - 1;
                else if (xTab[2] == n)
                    xTab[2] = 0;

                if (yTab[0] == -1)
                    yTab[0] = m - 1;
                else if (yTab[2] == m)
                    yTab[2] = 0;
            }

            else
            {
                if (xTab[0] == -1)
                    xTab[0] = 0;
                else if (xTab[2] == n)
                    xTab[2] = n - 1;

                if (yTab[0] == -1)
                    yTab[0] = 0;
                else if (yTab[2] == m)
                    yTab[2] = m - 1;
            }
            Color ownerColor = tab[x, y].getColor();
            for (int i = 0; i < xTab.Length; i++)
            {
                for (int j = 0; j < yTab.Length; j++)
                {
                    if (!(xTab[i] == x && yTab[j] == y) &&
                        !(xTab[i] == x - 1 && yTab[j] == y + 1) &&
                        !(xTab[i] == x + 1 && yTab[j] == y - 1))
                    {
                        if (tab[xTab[i], yTab[j]].isEmpty())
                        {
                            tab[xTab[i], yTab[j]].setColor(ownerColor);
                            tab[xTab[i], yTab[j]].setAlmostDone(true);
                        }
                    }
                }
            }
        }
        private void heksRightNbhd(int x, int y)
        {
            int[] xTab = { x - 1, x, x + 1 };       //tablica sasiadow
            int[] yTab = { y - 1, y, y + 1 };       //tablica sasiadow

            if (setBC)
            {
                if (xTab[0] == -1)
                    xTab[0] = n - 1;
                else if (xTab[2] == n)
                    xTab[2] = 0;

                if (yTab[0] == -1)
                    yTab[0] = m - 1;
                else if (yTab[2] == m)
                    yTab[2] = 0;
            }

            else
            {
                if (xTab[0] == -1)
                    xTab[0] = 0;
                else if (xTab[2] == n)
                    xTab[2] = n - 1;

                if (yTab[0] == -1)
                    yTab[0] = 0;
                else if (yTab[2] == m)
                    yTab[2] = m - 1;
            }
            Color ownerColor = tab[x, y].getColor();
            for (int i = 0; i < xTab.Length; i++)
            {
                for (int j = 0; j < yTab.Length; j++)
                {
                    if (!(xTab[i] == x && yTab[j] == y) &&
                        !(xTab[i] == x - 1 && yTab[j] == y - 1) &&
                        !(xTab[i] == x + 1 && yTab[j] == y + 1))
                    {
                        if (tab[xTab[i], yTab[j]].isEmpty())
                        {
                            tab[xTab[i], yTab[j]].setColor(ownerColor);
                            tab[xTab[i], yTab[j]].setAlmostDone(true);
                        }
                    }
                }
            }
        }
        private void pentDownNbhd(int x, int y)
        {
            int[] xTab = { x - 1, x, x + 1 };       //tablica sasiadow
            int[] yTab = { y - 1, y, y + 1 };       //tablica sasiadow

            if (setBC)
            {
                if (xTab[0] == -1)
                    xTab[0] = n - 1;
                else if (xTab[2] == n)
                    xTab[2] = 0;

                if (yTab[0] == -1)
                    yTab[0] = m - 1;
                else if (yTab[2] == m)
                    yTab[2] = 0;
            }

            else
            {
                if (xTab[0] == -1)
                    xTab[0] = 0;
                else if (xTab[2] == n)
                    xTab[2] = n - 1;

                if (yTab[0] == -1)
                    yTab[0] = 0;
                else if (yTab[2] == m)
                    yTab[2] = m - 1;
            }
            Color ownerColor = tab[x, y].getColor();
            for (int i = 0; i < xTab.Length; i++)
            {
                for (int j = 0; j < yTab.Length; j++)
                {
                    if (!(xTab[i] == x && yTab[j] == y) &&
                        !(xTab[i] == x - 1 && yTab[j] == y + 1) &&
                        !(xTab[i] == x && yTab[j] == y + 1) &&
                        !(xTab[i] == x + 1 && yTab[j] == y + 1))
                    {
                        if (tab[xTab[i], yTab[j]].isEmpty())
                        {
                            tab[xTab[i], yTab[j]].setColor(ownerColor);
                            tab[xTab[i], yTab[j]].setAlmostDone(true);
                        }
                    }
                }
            }
        }
        private void pentUpNbhd(int x, int y)
        {
            int[] xTab = { x - 1, x, x + 1 };       //tablica sasiadow
            int[] yTab = { y - 1, y, y + 1 };       //tablica sasiadow

            if (setBC)
            {
                if (xTab[0] == -1)
                    xTab[0] = n - 1;
                else if (xTab[2] == n)
                    xTab[2] = 0;

                if (yTab[0] == -1)
                    yTab[0] = m - 1;
                else if (yTab[2] == m)
                    yTab[2] = 0;
            }

            else
            {
                if (xTab[0] == -1)
                    xTab[0] = 0;
                else if (xTab[2] == n)
                    xTab[2] = n - 1;

                if (yTab[0] == -1)
                    yTab[0] = 0;
                else if (yTab[2] == m)
                    yTab[2] = m - 1;
            }
            Color ownerColor = tab[x, y].getColor();
            for (int i = 0; i < xTab.Length; i++)
            {
                for (int j = 0; j < yTab.Length; j++)
                {
                    if (!(xTab[i] == x && yTab[j] == y) &&
                        !(xTab[i] == x - 1 && yTab[j] == y - 1) &&
                        !(xTab[i] == x && yTab[j] == y - 1) &&
                        !(xTab[i] == x + 1 && yTab[j] == y - 1))
                    {
                        if (tab[xTab[i], yTab[j]].isEmpty())
                        {
                            tab[xTab[i], yTab[j]].setColor(ownerColor);
                            tab[xTab[i], yTab[j]].setAlmostDone(true);
                        }
                    }
                }
            }
        }
        private void pentLeftNbhd(int x, int y)
        {
            int[] xTab = { x - 1, x, x + 1 };       //tablica sasiadow
            int[] yTab = { y - 1, y, y + 1 };       //tablica sasiadow

            if (setBC)
            {
                if (xTab[0] == -1)
                    xTab[0] = n - 1;
                else if (xTab[2] == n)
                    xTab[2] = 0;

                if (yTab[0] == -1)
                    yTab[0] = m - 1;
                else if (yTab[2] == m)
                    yTab[2] = 0;
            }

            else
            {
                if (xTab[0] == -1)
                    xTab[0] = 0;
                else if (xTab[2] == n)
                    xTab[2] = n - 1;

                if (yTab[0] == -1)
                    yTab[0] = 0;
                else if (yTab[2] == m)
                    yTab[2] = m - 1;
            }
            Color ownerColor = tab[x, y].getColor();
            for (int i = 0; i < xTab.Length; i++)
            {
                for (int j = 0; j < yTab.Length; j++)
                {
                    if (!(xTab[i] == x && yTab[j] == y) &&
                        !(xTab[i] == x - 1 && yTab[j] == y - 1) &&
                        !(xTab[i] == x - 1 && yTab[j] == y) &&
                        !(xTab[i] == x - 1 && yTab[j] == y + 1))
                    {
                        if (tab[xTab[i], yTab[j]].isEmpty())
                        {
                            tab[xTab[i], yTab[j]].setColor(ownerColor);
                            tab[xTab[i], yTab[j]].setAlmostDone(true);
                        }
                    }
                }
            }
        }
        private void pentRightNbhd(int x, int y)
        {
            int[] xTab = { x - 1, x, x + 1 };       //tablica sasiadow
            int[] yTab = { y - 1, y, y + 1 };       //tablica sasiadow

            if (setBC)
            {
                if (xTab[0] == -1)
                    xTab[0] = n - 1;
                else if (xTab[2] == n)
                    xTab[2] = 0;

                if (yTab[0] == -1)
                    yTab[0] = m - 1;
                else if (yTab[2] == m)
                    yTab[2] = 0;
            }

            else
            {
                if (xTab[0] == -1)
                    xTab[0] = 0;
                else if (xTab[2] == n)
                    xTab[2] = n - 1;

                if (yTab[0] == -1)
                    yTab[0] = 0;
                else if (yTab[2] == m)
                    yTab[2] = m - 1;
            }
            Color ownerColor = tab[x, y].getColor();
            for (int i = 0; i < xTab.Length; i++)
            {
                for (int j = 0; j < yTab.Length; j++)
                {
                    if (!(xTab[i] == x && yTab[j] == y) &&
                        !(xTab[i] == x + 1 && yTab[j] == y - 1) &&
                        !(xTab[i] == x + 1 && yTab[j] == y) &&
                        !(xTab[i] == x + 1 && yTab[j] == y + 1))
                    {
                        if (tab[xTab[i], yTab[j]].isEmpty())
                        {
                            tab[xTab[i], yTab[j]].setColor(ownerColor);
                            tab[xTab[i], yTab[j]].setAlmostDone(true);
                        }
                    }
                }
            }
        }
        private void pentRandNbhd(int x, int y)
        {
            int choice = random.Next(1,4);
            Console.WriteLine("Wylosowano: " + choice);
            switch (choice)
            {
                case 1: pentDownNbhd(x, y);
                    break;
                case 2: pentUpNbhd(x, y);
                    break;
                case 3: pentLeftNbhd(x, y);
                    break;
                case 4: pentRightNbhd(x, y);
                    break;
            }
        }
        private void heksRandNbhd(int x, int y)
        {
            int choice = random.Next(1, 3);
            switch (choice)
            {
                case 1: heksLeftNbhd(x, y);
                    break;
                case 2: heksRightNbhd(x, y);
                    break;
            }
        }

        private void cleaner(int x, int y)
        {
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    tab[i, j].setEmpty(true);
                    tab[i, j].setAlmostDone(false);
                }
            }
            this.Refresh();
        }
        private void startButton_Click(object sender, EventArgs e)
        {
            doMagic();
        }
        private void cleanButton_Click(object sender, EventArgs e)
        {
            nbhdChoice = 0;
            cleaner(n, m);
            textBox1.Text = String.Empty;
            textBox2.Text = String.Empty;
            foreach (int i in checkedListBox1.CheckedIndices)
            {
                checkedListBox1.SetItemCheckState(i, CheckState.Unchecked);
            }
            foreach (int i in checkedListBox2.CheckedIndices)
            {
                checkedListBox2.SetItemCheckState(i, CheckState.Unchecked);
            }
        }
        private void setButton_Click(object sender, EventArgs e)
        {
            switch (locationChoice)
            {
                case 1:
                    drawRand();
                    break;
                case 2:
                    drawRegular();
                    break;
                case 3:
                    drawCircle();
                    break;
                default:
                    MessageBox.Show("Select the type of location");
                    break;
            }
        }

        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e)
        {

            for (int ix = 0; ix < checkedListBox1.Items.Count; ++ix)
                if (ix != e.Index) checkedListBox1.SetItemChecked(ix, false);

            if (checkedListBox1.SelectedItem.ToString() == "Moore")
            {
                nbhdChoice = 1;
            }
            else if (checkedListBox1.SelectedItem.ToString() == "von Neumann")
            {
                nbhdChoice = 2;
            }
            else if (checkedListBox1.SelectedItem.ToString() == "Heksagonal Left")
            {
                nbhdChoice = 3;
            }
            else if (checkedListBox1.SelectedItem.ToString() == "Heksagonal Right")
            {
                nbhdChoice = 4;
            }
            else if (checkedListBox1.SelectedItem.ToString() == "Heksagonal Rand")
            {
                nbhdChoice = 5;
            }
            else if (checkedListBox1.SelectedItem.ToString() == "Pentagonal Rand")
            {
                nbhdChoice = 6;
            }
            Console.WriteLine("Choice: " + nbhdChoice);
        }
        private void checkedListBox2_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            for (int ix = 0; ix < checkedListBox2.Items.Count; ++ix)
                if (ix != e.Index) checkedListBox2.SetItemChecked(ix, false);

            if (checkedListBox2.SelectedItem.ToString() == "Losowe")
            {
                locationChoice = 1;
            }
            else if (checkedListBox2.SelectedItem.ToString() == "Równomierne")
            {
                locationChoice = 2;
            }
            else if (checkedListBox2.SelectedItem.ToString() == "Losowe z promieniem R")
            {
                locationChoice = 3;
            }
        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)

        {
            if (checkBox1.Checked)
            {
                Console.WriteLine("checked");
                setBC = true;
            }

            else
            {
                Console.WriteLine("unchecked");
                setBC = false;
            }
            Console.WriteLine(setBC);
        }
        private void grid_checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (grid_checkBox2.Checked)
            {
                Console.WriteLine("checked");
                gridFlag = true;
            }

            else
            {
                Console.WriteLine("unchecked");
                gridFlag = false;
            }
        }

        private void drawRegular()
        {
            if (string.IsNullOrWhiteSpace(textBox1.Text))
            {
                MessageBox.Show("Wpisz wartosc do textBoxa");
            }
            else
            {
                int textBoxValue = Convert.ToInt32(textBox1.Text);

                if (textBoxValue > n || textBoxValue > m)
                {
                    MessageBox.Show("Set other grain amount & radius!");
                    cleaner(n, m);
                }
                else
                {
                    for (int i = n / 2; i < n; i += textBoxValue)
                    {
                        for (int j = m / 2; j < m; j += textBoxValue)
                        {
                            Color randomColor = Color.FromArgb(random.Next(255), random.Next(255),
                            random.Next(255));
                            tab[i, j].setEmpty(false);
                            tab[i, j].setColor(randomColor);
                        }
                    }
                    for (int i = n / 2; i > -1; i -= textBoxValue)
                    {
                        for (int j = m / 2; j < m; j += textBoxValue)
                        {
                            Color randomColor = Color.FromArgb(random.Next(255), random.Next(255),
                            random.Next(255));
                            tab[i, j].setEmpty(false);
                            tab[i, j].setColor(randomColor);
                        }
                    }
                    for (int i = n / 2; i < n; i += textBoxValue)
                    {
                        for (int j = m / 2; j > -1; j -= textBoxValue)
                        {
                            Color randomColor = Color.FromArgb(random.Next(255), random.Next(255),
                            random.Next(255));
                            tab[i, j].setEmpty(false);
                            tab[i, j].setColor(randomColor);
                        }
                    }
                    for (int i = n / 2; i > -1; i -= textBoxValue)
                    {
                        for (int j = m / 2; j > -1; j -= textBoxValue)
                        {
                            Color randomColor = Color.FromArgb(random.Next(255), random.Next(255),
                            random.Next(255));
                            tab[i, j].setEmpty(false);
                            tab[i, j].setColor(randomColor);
                        }
                    }
                }

            }
        }
        private void drawRand()
        {

            if (string.IsNullOrWhiteSpace(textBox1.Text))
            {
                MessageBox.Show("Wpisz wartosc do textBoxa");
            }
            else
            {
                int textBoxValue = Convert.ToInt32(textBox1.Text);
                if (textBoxValue > n * m)
                {
                    MessageBox.Show("Setted grain amount is bigger than matrix. Change amount!");
                    cleaner(n, m);
                }
                else
                {
                    for (int a = textBoxValue; a > 0; a--)
                    {
                        int i = random.Next(n);
                        int j = random.Next(m);

                        Color randomColor = Color.FromArgb(random.Next(255), random.Next(255),
                            random.Next(255));
                        tab[i, j].setEmpty(false);
                        tab[i, j].setColor(randomColor);
                    }
                }

            }
        }
        private void drawCircle()
        {
            cleaner(n, m);

            if (string.IsNullOrWhiteSpace(textBox1.Text))
            {
                MessageBox.Show("Set the grain amount");
            }
            else if (string.IsNullOrWhiteSpace(textBox2.Text))
            {
                MessageBox.Show("Set the radius");
            }
            else
            {
                bool flag = true;
                int result = 0;
                int amount = Convert.ToInt32(textBox1.Text);
                int radius = Convert.ToInt32(textBox2.Text) + 1;
                int i = random.Next(n);
                int j = random.Next(m);

                randTimer = new System.Timers.Timer();
                randTimer.Interval = 1000;
                randTimer.Elapsed += new ElapsedEventHandler(this.timer1_Tick);

                grainList.Add(new Grain(i, j));
                Color randomColor = Color.FromArgb(random.Next(255), random.Next(255),
                                            random.Next(255));
                tab[i, j].setEmpty(false);
                tab[i, j].setColor(randomColor);

                while (grainList.Count < amount)
                {
                    randTimer.Start();

                    Grain newGrain = new Grain(random.Next(n), random.Next(m));
                    foreach (Grain addedGrain in grainList)                 //addedGrain to kazdy kolejny
                    {
                        result = countDistance(addedGrain, newGrain);
                        if (result < radius)
                            flag = false;
                    }
                    if (flag)
                    {
                        grainList.Add(newGrain);
                        randomColor = Color.FromArgb(random.Next(255), random.Next(255),
                            random.Next(255));
                        tab[newGrain.i, newGrain.j].setEmpty(false);
                        tab[newGrain.i, newGrain.j].setColor(randomColor);

                        randTimer.Stop();
                    }
                    flag = true;

                    if (!timerFlag)
                    {
                        MessageBox.Show("Set other grain amount & radius!");
                        cleaner(n, m);
                        break;
                    }
                }
            }
        }
        public int countDistance(Grain g1, Grain g2)
        {
            int distance = (int)Math.Sqrt(Math.Pow(g2.i - g1.i, 2) + Math.Pow(g2.j - g1.j, 2));
            return distance;
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            timeLeft--;
            if (timeLeft == 0)
            {
                randTimer.Stop();
                timerFlag = false;
            }
        }

        private void drawStill()
        {
            stillTimer = new System.Timers.Timer();
            stillTimer.Interval = 1000;
            stillTimer.Elapsed += new ElapsedEventHandler(this.stillTimerFunction);
        }
        private void stillTimerFunction(object sender, EventArgs e)
        {
            if (stillFlag)
            {
                int i = random.Next(n);
                int j = random.Next(m);

                if (tab[i, j].isEmpty())
                {
                    Color randomColor = Color.FromArgb(random.Next(255), random.Next(255),
                            random.Next(255));
                    tab[i, j].setEmpty(false);
                    Console.WriteLine("Time = " + stillTimer.Interval + "tab[i,j] = "+ tab[i, j].isEmpty());
                    tab[i, j].setColor(randomColor);
                    tab[i, j].setAlmostDone(true);
                }
            }
            
        }
        private void stillStartButton_Click(object sender, EventArgs e)
        {
            drawStill();
            stillTimer.Start();
            stillFlag = true;
        }
        private void stillStopButton_Click(object sender, EventArgs e)
        {
            stillTimer.Stop();
            stillFlag = false;
        }

        //rekrystalizacja
        private bool isOnTheBorder(int x, int y)
        {
            int[] xTab = { x - 1, x, x + 1 };
            int[] yTab = { y - 1, y, y + 1 };

            if (setBC)
            {
                if (xTab[0] == -1)
                    xTab[0] = n - 1;
                else if (xTab[2] == n)
                    xTab[2] = 0;

                if (yTab[0] == -1)
                    yTab[0] = m - 1;
                else if (yTab[2] == m)
                    yTab[2] = 0;
            }
            else
            {
                if (xTab[0] == -1)
                    xTab[0] = 0;
                else if (xTab[2] == n)
                    xTab[2] = n - 1;

                if (yTab[0] == -1)
                    yTab[0] = 0;
                else if (yTab[2] == m)
                    yTab[2] = m - 1;
            }

            Color myColor = tab[x, y].getColor();
            for (int i = 0; i < xTab.Length; i++)
            {
                for (int j = 0; j < yTab.Length; j++)
                {
                    if (!(xTab[i] == x && yTab[j] == y))
                    {
                        Color nbhdColor = tab[xTab[i], yTab[j]].getColor();
                        if (myColor != nbhdColor)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        private double countRo(double T)
        {
            ro = A / B + (1 - A / B) * Math.Exp(-B * T);
            return ro;
        }
        private void changeBorderGrainState(int i, int j, List<Grain> lista, Color color)
        {
            foreach (Grain borderGrain in lista)
            {
                tab[i, j].setColor(color);
            }
        }
        private void recrystallizationButton_Click(object sender, EventArgs e)
        {
            List<Grain> borderGrainsList = new List<Grain>();
            time += step;
            ro = countRo(time) - countRo(time - step);
            double singleCellRo = ro / (n * m);
            double restRo = ro;
            double singleBorderCellRo = 0;

            checkIfNbhdRecrystalized();
           
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    changeRecState(i, j, changeRecrystalizedGrainColor);
                }
            }
            changeRecrystalizedGrainColor.Clear();

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    if (isOnTheBorder(i, j) && !(tab[i,j].isRecrystalized()))
                    {
                        tab[i, j].setRo(tab[i, j].getRo() + (0.8 * singleCellRo));
                        restRo -= 0.8 * singleCellRo;
                        borderGrainsList.Add(new Grain(i, j));
                    }
                    else
                    {
                        tab[i, j].setRo(tab[i, j].getRo() + (0.2 * singleCellRo));
                        restRo -= 0.2 * singleCellRo;
                    }
                }
            }

            int randomCellsForRestRo = 2;
            singleBorderCellRo = restRo / randomCellsForRestRo;
            if (borderGrainsList.Count > 2)
            {
                for (int i = 0; i < randomCellsForRestRo; i++)
                {
                    Grain borderGrain = borderGrainsList[random.Next(borderGrainsList.Count)];
                    tab[borderGrain.i, borderGrain.j].setRo(tab[borderGrain.i, borderGrain.j].getRo() + singleBorderCellRo);
                }
            }
            else
            {
                Console.WriteLine("NIE PRZESZKADZAJ, DZIAŁA TO DZIAŁA");
            }
   
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    if (tab[i, j].getRo() > criticalRo)
                    {
                        Color randomColor = Color.FromArgb(random.Next(255), random.Next(255),
                           random.Next(255));
                        changeBorderGrainState(i, j, borderGrainsList, randomColor);
                        tab[i, j].setRecrystalized(true);
                        tab[i, j].setRo(0);
                    }
                }
            }
            this.Refresh();
        }
        private void changeRecState(int i, int j, Dictionary<Grain, Color> lista)
        {
            foreach (KeyValuePair<Grain, Color> recGrain in lista)
            {
                Grain index = recGrain.Key;
                Color grainColor = recGrain.Value;
                tab[index.i, index.j].setColor(grainColor);
                tab[index.i, index.j].setRecrystalized(true);
                tab[index.i, index.j].setRo(0);
            }
        }
        private void checkIfNbhdRecrystalized()
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    if (tab[i, j].isRecrystalized())
                    {
                        MooreRec(i, j);
                    }
                }
            }
        }
        private void MooreRec(int x, int y)
        {
            int[] xTab = { x - 1, x, x + 1 };
            int[] yTab = { y - 1, y, y + 1 };

            if (setBC)
            {
                if (xTab[0] == -1)
                    xTab[0] = n - 1;
                else if (xTab[2] == n)
                    xTab[2] = 0;

                if (yTab[0] == -1)
                    yTab[0] = m - 1;
                else if (yTab[2] == m)
                    yTab[2] = 0;
            }
            else
            {
                if (xTab[0] == -1)
                    xTab[0] = 0;
                else if (xTab[2] == n)
                    xTab[2] = n - 1;

                if (yTab[0] == -1)
                    yTab[0] = 0;
                else if (yTab[2] == m)
                    yTab[2] = m - 1;
            }

            Color ownerColor = tab[x, y].getColor();
            for (int i = 0; i < xTab.Length; i++)
            {
                for (int j = 0; j < yTab.Length; j++)
                {
                    if (!(xTab[i] == x && yTab[j] == y))
                    {
                        if (!(tab[xTab[i], yTab[j]].isRecrystalized()))
                        {
                            changeRecrystalizedGrainColor.Add(new Grain(xTab[i], yTab[j]),ownerColor);
                        }
                    }
                }
            }
        }
        //rekrystalizacja

        //monte carlo - zmienic n i m na mniejsze (12x12) zeby było widać jakieś ładne efekty
        private void monteCarloButton_Click(object sender, EventArgs e)
        {
            Brush brush;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Color monteCarloColor = Color.FromArgb(random.Next(255), random.Next(255),
                            random.Next(255));
                    brush = new SolidBrush(monteCarloColor);
                    gObject.FillRectangle(brush, i * 8, j * 8, 8, 8);
                    tab[i, j].setEmpty(false);
                    tab[i, j].setColor(monteCarloColor);
                    randomMCGrainList.Add(new Grain(i, j));
                }
            }
        }
        private void stepMC()
        {
            var result = randomMCGrainList.OrderBy(item => random.Next());
            foreach (Grain myGrain in result)
            {
                if(isOnTheBorder(myGrain.i,myGrain.j)){
                    compareEnergy(myGrain);
                }
            }
        }
        private void compareEnergy(Grain grain)
        {
            int oldEnergy = 0;
            int newEnergy = 0;

            List<Grain> neibsMClist = new List<Grain>();
            getNeibs(grain.i, grain.j, neibsMClist);

            Color randGrainColor = tab[grain.i, grain.j].getColor();
            oldEnergy = calcEnergy(randGrainColor, neibsMClist);

            Grain randNbhdGrain = neibsMClist[random.Next(neibsMClist.Count)];
            Color randNbhdGrainColor = tab[randNbhdGrain.i, randNbhdGrain.j].getColor();

            newEnergy = calcEnergy(randNbhdGrainColor, neibsMClist);

            if (newEnergy <= oldEnergy)
            {
                tab[grain.i, grain.j].setColor(tab[randNbhdGrain.i, randNbhdGrain.j].getColor());
                this.Refresh();
            }

        }
        private void getNeibs(int x, int y, List<Grain> neibs)
        {

            int[] xTab = { x - 1, x, x + 1 };
            int[] yTab = { y - 1, y, y + 1 };
            
            if (setBC)
            {
                if (xTab[0] == -1)
                    xTab[0] = n - 1;
                else if (xTab[2] == n)
                    xTab[2] = 0;

                if (yTab[0] == -1)
                    yTab[0] = m - 1;
                else if (yTab[2] == m)
                    yTab[2] = 0;
            }
            else
            {
                if (xTab[0] == -1)
                    xTab[0] = 0;
                else if (xTab[2] == n)
                    xTab[2] = n - 1;

                if (yTab[0] == -1)
                    yTab[0] = 0;
                else if (yTab[2] == m)
                    yTab[2] = m - 1;
            }

            for (int i = 0; i < xTab.Length; i++)
            {
                for (int j = 0; j < yTab.Length; j++)
                {
                    if (!(xTab[i] == x && yTab[j] == y))
                    { 
                        neibs.Add(new Grain(xTab[i],yTab[j]));
                    }
                }
            }
        }
        private int calcEnergy(Color ownerColor, List<Grain> neibs)
        {
            int counter = 0;

            foreach (Grain borderGrain in neibs)
            {
                if (ownerColor != tab[borderGrain.i, borderGrain.j].getColor())
                    counter++;
            }
            return counter;
        }
        private void oneMCstepButton_Click(object sender, EventArgs e)
        {
            stepMC();
            //this.Refresh();
        }
        //monte carlo
    }
}
