﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace naive_grain_growth
{
    public class Cell
    {
        bool empty = true;
        bool almostDone = false;
        Color color;
        public double ro=0;
        bool recrystalized = false;
        public int i, j;

        //public Cell(int i, int j)
        //{
        //    this.i = i;
        //    this.j = j;
        //}

        public void setColor(Color color)
        {
            this.color = color;
        }
        public Color getColor()
        {
            return color;

        }

        public void setEmpty(bool empty)
        {
            this.empty = empty;
        }
        public bool isEmpty()
        {
            return empty;

        }

        public void setAlmostDone(bool almostDone)
        {
            this.almostDone = almostDone;
        }
        public bool isAlmostDone()
        {
            return almostDone;

        }

        public void setRo(double ro)
        {
            this.ro = ro;
        }
        public double getRo()
        {
            return ro;
        }

        public void setRecrystalized(bool recrystalized)
        {
            this.recrystalized = recrystalized;
        }
        public bool isRecrystalized()
        {
            return recrystalized;

        }
    }
}
